# start keychain 
if [[ -z $(pidof ssh-agent) && -z $(pidof gpg-agent) ]]; then
  eval $(/usr/bin/keychain --eval -Q -q --nogui --agents "ssh,gpg" id_rsa aur)
  [[ -z $HOSTNAME ]] && HOSTNAME=$(uname -n)
  [[ -f $HOME/.keychain/${HOSTNAME}-sh ]] && source $HOME/.keychain/${HOSTNAME}-sh
  [[ -f $HOME/.keychain/${HOSTNAME}-sh-gpg ]] && source $HOME/.keychain/${HOSTNAME}-sh-gpg
fi

# startx if on tty1 and tmux on tty2
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
  exec startx -- vt1 -keeptty &>/dev/null
  logout
elif [[ $(tty) = /dev/tty2 ]]; then
  tmux new -s secured
fi
