#!/bin/sh

while test $# -gt 0
do
    case "$1" in
        --off) echo "2-3" | sudo tee /sys/bus/usb/drivers/usb/unbind
            ;;
        --on) echo "2-3" | sudo tee /sys/bus/usb/drivers/usb/bind
            ;;
        --*) echo "Argument error"
    esac
    shift
done

exit 0
