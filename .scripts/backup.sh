#!/usr/bin/env sh

lan="$(ip addr | grep 168)"
back="/media/backup/"
dirs=("$HOME/.config" )

if [[ -z $lan ]]; then 
    printf "%s\n" "Not on home LAN; exiting?"
    exit 1
fi

if [[ -d $back ]]; then
    printf "%s\n" "Already mounted..."
else
    sudo mount -a
fi

printf "%s\n" "Beginning sync..."

rsync -Cazs --stats "${dirs[@]}" "${back}"

exit 0
