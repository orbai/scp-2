#!/usr/bin/env sh

# feh script to set the desired dynamic wallpaper

dune_phase () {
    hour=$(date +%H)
    img_loc=$HOME/pictures/wallpapers/mojave
    if [ $hour -eq 8 ]; then
        feh --bg-fill $img_loc/1.jpeg
    elif [ $hour -eq 9 ]; then
        feh --bg-fill $img_loc/2.jpeg
    elif [ $hour -eq 10 ]; then
        feh --bg-fill $img_loc/3.jpeg
    elif [ $hour -eq 11 ]; then
        feh --bg-fill $img_loc/4.jpeg
    elif [ $hour -eq 12 ]; then
        feh --bg-fill $img_loc/5.jpeg
    elif [ $hour -eq 13 ]; then
	feh --bg-fill $img_loc/6.jpeg
    elif [ $hour -eq 14 ]; then
	feh --bg-fill $img_loc/6.jpeg
    elif [ $hour -eq 15 ]; then
        feh --bg-fill $img_loc/7.jpeg
    elif [ $hour -eq 16 ]; then
        feh --bg-fill $img_loc/8.jpeg
    elif [ $hour -eq 17 ]; then
        feh --bg-fill $img_loc/9.jpeg
    elif [ $hour -eq 18 ]; then
	feh --bg-fill $img_loc/9.jpeg
    elif [ $hour -eq 19 ]; then
        feh --bg-fill $img_loc/10.jpeg
    elif [ $hour -eq 20 ]; then
        feh --bg-fill $img_loc/11.jpeg
    elif [ $hour -eq 21 ]; then
        feh --bg-fill $img_loc/12.jpeg
    elif [ $hour -eq 22 ]; then
        feh --bg-fill $img_loc/13.jpeg
    elif [ $hour -eq 23 ]; then
        feh --bg-fill $img_loc/14.jpeg
    elif [ $hour -eq 24 ]; then
        feh --bg-fill $img_loc/15.jpeg
    else
        feh --bg-fill $img_loc/16.jpeg
    fi
}

moon_phase () {
    hour=$(date +%e)
    img_loc=$HOME/pictures/wallpapers/moon
    if [  $hour -lt 10  ]; then
        feh --bg-fill $img_loc/1.png
    elif [ $hour -lt 12  ]; then
        feh --bg-fill $img_loc/2.png
    elif [ $hour -lt 14  ]; then
        feh --bg-fill $img_loc/3.png
    elif [ $hour -lt 16  ]; then
        feh --bg-fill $img_loc/4.png
    elif [ $hour -lt 20  ]; then
        feh --bg-fill $img_loc/5.png
    else
        feh --bg-fill $img_loc/6.png
    fi
}

catalina () {
	hour=$(date +%H)
	img_loc=$HOME/pictures/wallpapers/catalina
	if [ $hour -lt 20 ]; then
		feh --bg-fill $img_loc/catalina-wallpaper-light.png
	else
		feh --bg-fill $img_loc/catalina-wallpaper-dark.png
	fi
}

nature () {
	img_loc=$HOME/pictures/wallpapers/nature
	feh --randomize --bg-fill $img_loc/*
}

while [ $# -gt 0 ]
do
    if [[ $1 == "mojave" ]];
        then
            dune_phase
    elif [[ $1 == "moon" ]];
        then
            moon_phase
    elif [[ $1 == "catalina" ]];
        then
            catalina
    elif [[ $1 == "nature" ]];
        then
            nature
    fi
    shift
done