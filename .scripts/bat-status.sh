#!/usr/bin/env zsh
# Status script for dwm

# The MIT License (MIT) http://opensource.org/licenses/MIT

# Time in seconds between checks/messages.
#  Common values:
#  300  = 5  mins
#  600  = 10 mins
#  1800 = 30 mins
#  3600 = 1  hour

WAIT_TIME=300
RPID=9901

# Icons
CRITICAL_ICON=/home/orbai/.config/twmn/icons/critical.png
INFO_ICON=$HOME/.config/twmn/icons/info.png
WARNING_ICON=$HOME/.config/twmn/icons/warning.png


while true; do
  # Get statuses
  AC="$(</sys/class/power_supply/AC/online)"
  BAT0="$(</sys/class/power_supply/BAT0/capacity)"
  BAT1="$(</sys/class/power_supply/BAT1/capacity)"

  if [[ $AC -eq 0 && BAT0 -eq 1 ]] && [[ $AC -eq 0 && $BAT1 -eq 1 ]];
    then
      systemctl suspend
  elif [[ $AC -eq 0 && BAT0 -lt 5 ]] && [[ $AC -eq 0 && $BAT1 -lt 5 ]];
    then
      dunstify -u critical -p $RPID -i $CRITICAL_ICON -a Battery 'Batteries are lower than 5%, connect your charger!'
  elif [[ $AC -eq 0 && BAT0 -lt 10 ]] && [[ $AC -eq 0 && $BAT1 -lt 10 ]];
    then
      dunstify -u normal -p $RPID -i $WARNING_ICON -a Battery 'Batteries are lower than 10%, connect your charger!'
  elif [[ $AC -eq 0 && BAT0 -lt 15 ]] && [[ $AC -eq 0 && $BAT1 -lt 15 ]];
    then
      dunstify -u info -p $RPID -i $INFO_ICON -a BAttery 'Batteries are low, connect your charger!'
  fi
sleep $WAIT_TIME

done
