#!/usr/bin/env bash
# Script for installing archlinux on un/encrypted lvm (part1)
# Orbai B Sebastian
# The MIT License (MIT) http://opensource.org/licenses/MIT

# Disclaimer
# Using this script does not guarantee a succesful installation, use it on your own risk

YELLOW='\033[1;33m'
RED='\033[0;31m'
NC='\033[0m'

## Setup partitions ##
read -p "Enter target installation disk:" DISK
# Create boot partition
(echo g; echo n; echo 1; echo  ; echo +512M; echo t; echo 1; echo w) | fdisk $DISK
BOOT_PART="${DISK}p1"
(echo n; echo ; echo ; echo ; echo t; echo 2; echo 31; echo w) | fdisk $DISK
LVM_PART="${DISK}p2"

# Setup encryption
while :
do
	read -p "Do you want encryption? (y/n)" CRYPT
	if [ "$CRYPT" = "y" ]; then
		cryptsetup -c aes-xts-plain64 -y -s 512 luksFormat $LVM_PART
		echo "Decrypting partition..."
		sleep 5
		cryptsetup luksOpen $part luks
		break
	elif [ "$CRYPT" = "n" ]; then
		echo "Partition will remain unencrypted"
		break
	else
		echo "Enter y or n"
		continue
	fi
done

# Create LVM
pvcreate $LVM_PART
sleep 2
vgcreate vg0 $LVM_PART
read -p "Please enter size of root  partition (ex 40G): " SZ1
lvcreate --size $SZ1 vg0 --name root
printf "${YELLOW}Creating /home and consuming rest of the hard-drive...${NC}\n"
sleep 1
lvcreate -l +100%FREE vg0 --name home
sleep 3

# Format devices
mkfs.fat -F32 $BOOT_PART
mkfs.ext4 /dev/mapper/vg0-root
mkfs.ext4 /dev/mapper/vg0-home

# Set up Mount points
mount /dev/mapper/vg0-root /mnt
mkdir -p /mnt/home
mkdir -p /mnt/boot/EFI
mount /dev/mapper/vg0-home /mnt/home
mount $BOOT_PART /mnt/boot/EFI

# Install base arch
pacstrap -i /mnt base base-devel grub

# Install extra stuff
pacstrap -i /mnt mercurial zsh git xorg wget networkmanager dialog wpa_supplicant nitrogen lxappearance thunar compton dmenu xterm network-manager-applet ttf-dejavu iw efibootmgr dosfstools os-prober mtools

# lvmetad service
printf "${YELLOW}Enabling lvmetad${NC}\n"
sleep 2
systemctl start lvm2-lvmetad.service

# Generate fstab file
printf "${YELLOW}Generating fstab...${NC}\n"
/bin/genfstab -U -p /mnt >> /mnt/etc/fstab
sleep 2
cat /mnt/etc/fstab
sleep 10

printf "${YELLOW}All processes completed. Please run 'arch-chroot /mnt' and continue with install process.${NC}\n"
sleep 2

exit 0
