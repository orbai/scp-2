#!/usr/bin/env bash
# Script for installing archlinux on encrypted lvm (part2)
# Orbai B Sebastian
# The MIT License (MIT) http://opensource.org/licenses/MIT

# Disclaimer
# Using this script does not guarantee a succesful installation, use it on your own risk

YELLOW='\033[1;33m'
RED='\033[0;31m'
NC='\033[0m'

# Configuring locale
rm -rf /etc/locale.gen
echo "en_US ISO-8859-1" >> /etc/locale.gen
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
/bin/locale-gen


# Set needed console font
setfont Lat2-Terminus16
echo 'FONT=Lat2-Terminus16' > /etc/vconsole.conf
echo 'KEYMAP=us' >> /etc/vconsole.conf


# Configure localtime
ln -s /usr/share/zoneinfo/Europe/Bucharest /etc/localtime
hwclock --systohc --utc

# Set hostname
read -p  "Enter machine hostname: " HOST
echo $HOST > /etc/hostname

# Set up the root password
printf "${YELLOW}Setting up the root password${NC}\n"
sleep 1
/bin/passwd

# Set up username
printf "${YELLOW}Setting up a username${NC}\n"
sleep 2
read -p "Enter new username: " USER
/bin/useradd -m -g users -G wheel,storage,power,network -s /bin/bash $USER
/bin/passwd $USER

# Re-generate linux image
printf "${YELLOW}Adding necesary modules and re-generate linux image${NC}\n"
sleep 5
while :
do
    read -p "Did you select an encrypted partition in previous script? Choose carefully!(y/n) " ANSW
    if [ "$ANSW" = "y" ]; then
      echo "Modifying /etc/mkinitcpio.conf and adding crypto option..."
      sleep 2
      sed -i.bak 's/block/& keymap encrypt lvm2/' /etc/mkinitcpio.conf
      break
    elif [ "$ANSW" = "n" ]; then
			printf "${YELLOW}Modifying /etc/mkinitcpio.conf with lvm2 option${NC}\n"
			sleep 2
			sed -i.bak 's/block/& lvm2/' /etc/mkinitcpio.conf
			break
    else
      echo "No or incorrect selection"
      continue
    fi
done

mkinitcpio -p linux


# Configure grub
grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck
grub-mkconfig -o /boot/grub/grub.cfg

# Final step
printf "${YELLOW}All done! Unmount volumes, and reboot.${NC}\n"
printf "${YELLOW}Remove your Arch Linux live CD when your computer powers on again.${NC}\n"
exit 0
