####### General #######
alias makedwm="cd ~/suckless/dwm && sudo rm -rf config.h && sudo make uninstall && sudo make install clean && cd"
alias vim="nvim"
alias vimdiff="nvim -d"
alias tmux="tmux -f ~/.tmux/conf"
alias mutt="mutt -F ~/.mutt/muttrc"
alias pi="ssh scp-3"
alias gpgc="gpg -c"
alias slack-term="slack-term -config ~/.config/slack/term_config"
alias svim="sudo nvim"
alias turf="tabbed -c surf -e"
alias fullcharge="~/.scripts/fullcharge.sh"

#The 'ls' family
alias ll="ls -l --group-directories-first"
alias ls="ls -h --color"    # add colors for filetype recognition
alias la="ls -A"            # show hidden files
alias lx="ls -xb"           # sort by extension
alias lk="ls -lSr"          # sort by size, biggest last
alias lc="ls -ltcr"         # sort by and show change time, most recent last
alias lu="ls -ltur"         # sort by and show access time, most recent last
alias lt="ls -ltr"          # sort by date, most recent last
alias lm="ls -Al |more"     # pipe through 'more'
alias lr="ls -lR"           # recursive ls
alias lsr="tree -Csu"       # nice alternative to 'recursive ls'

######## Pacman ########
# update
alias pacup="sudo pacman -Syu"

# Remove orphans
alias orphans="pacman -Qtdq"
