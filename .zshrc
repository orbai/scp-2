HISTFILE=~/.histfile~
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd nomatch notify
unsetopt beep extendedglob

# key bindings
bindkey -e
bindkey "${terminfo[khome]}" beginning-of-line
bindkey "${terminfo[kend]}" end-of-line
bindkey "\e[3~" delete-char


zstyle :compinstall filename '/home/orbai/.zshrc'

autoload -Uz compinit
compinit

# colour coreutils
export GREP_COLOR="1;31"
alias grep="grep --color=auto"
alias "ls=ls --color=auto"

# colors for ls
test -r "~/.dir_colors" && eval $(dircolors ~/.dir_colors)

export EDITOR=nvim
export DE=GNOME
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

# point Zsh at the right dotfiles
ZDOTDIR="${ZDOTDIR:-$HOME/.zshenv}"

# functions
if [[ -d "$ZDOTDIR" ]]; then
  for file in "$ZDOTDIR"/*.zsh; do
    source "$file"
  done
fi
